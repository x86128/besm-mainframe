`include "besm_cpu.vh"
`default_nettype none

module besm_decode
(
    input  [23:0] instr,

    output         is_long,
    output         is_mag,
    output [3:0]   op_idx,
    output [6:0]   op_code,
    output [14:0]  op_addr
);

    assign is_long   = instr[19];

    assign op_idx    = instr[23:20];
    assign op_code   = is_long ? {{3'b100},{instr[18:15]}} : {{1'b0},{instr[17:12]}};
    assign op_addr   = is_long ? instr[14:0] : {{3{instr[18]}},{instr[11:0]}};

    assign is_mag = op_addr == 0 && op_idx == 15;


endmodule