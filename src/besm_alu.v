`default_nettype none

module norm(
    input         i_clk,
    input         i_rst,

    input         i_pbn,
    input         i_pbo,
    input [47:0]  i_acc,
    input [47:0]  i_rmr,
    input         i_valid,

    output [47:0] o_acc,
    output [47:0] o_rmr,
    output reg    o_valid );

    localparam ST_IDLE = 0,
               ST_LOOP = 1,
               ST_ROUND = 2,
               ST_PREPARE = 3,
               ST_NORMALIZE = 4,
               ST_EXIT = 5;

    reg [4:0] state;

    reg [47:0] acc;
    reg [47:0] rmr;

    reg m;

    always @(posedge i_clk) begin
        if (i_rst)
            state <= ST_IDLE;
        else
            case (state) 
                ST_IDLE: begin
                    if (i_valid) begin
                        m       <= 0;
                        acc     <= i_acc;
                        rmr     <= i_rmr;
                        state   <= ST_PREPARE;
                        o_valid <= 0;
                    end
                end
                ST_PREPARE: begin
                    if (i_pbn)
                        state <= ST_ROUND;
                    else
                        state <= ST_NORMALIZE;
                end
                ST_NORMALIZE: begin
                    if (acc[39] == acc[40]) begin
                        acc[39:0] <= {acc[38:0], rmr[39]};
                        if (rmr[39] == 1)
                            m <= 1;
                        rmr[39:0]  <= {rmr[38:0],1'b0};
                        if ((acc[47:41]-1) == 0) begin
                            acc[47:0] <= 0;
                            state <= ST_EXIT;
                        end else begin
                            acc[47:41] <= acc[47:41]-1;
                            state <= ST_NORMALIZE;
                        end
                    end else begin
                        state <= ST_ROUND;
                    end
                end
                ST_ROUND: begin
                    if (i_pbo || m || rmr[39:0] == 0)
                        state <= ST_EXIT;
                    else begin
                        acc[0] = 1;
                        state <= ST_EXIT;
                    end
                end
                ST_EXIT: begin
                    o_valid <= 1;
                    state <= ST_IDLE;
                end
                default:
                    state <= ST_IDLE;
            endcase
        
    end

    assign o_acc = acc;
    assign o_rmr = rmr;
   
endmodule