// index registers
module mregs
(
    input         i_clk,
    input  [3:0]  wr_addr,
    input  [14:0] i_data,
    input         i_we,
    input  [3:0]  rd_addr,
    output [14:0] o_data
);

    reg [14:0] mregs[0:15];

    always @(posedge i_clk) begin
        if (i_we)
            mregs[wr_addr] <= i_data;
    end

    assign o_data = !rd_addr ? 0 : mregs[rd_addr];
    
endmodule