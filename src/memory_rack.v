`include "memory_rack.vh"
`default_nettype none

module memory_rack
(
    input 	      i_clk,
    input         i_rst_n,

    input 	      i_cmd,
    input         i_req,
    output        o_valid,
    
    input  [14:0] i_addr,
    input  [47:0] i_data,
    input  [3:0]  i_prg_num, // selected hardwired program
    
    output [47:0] o_data
);
    reg [47:0] 	mem[0:32767];
    reg [47:0] 	o_reg;

    wire [47:0] o_rom;

    // hardwired programs ROM
    pult_rom pult_rom
    (
        .i_addr    ( i_addr    ),
        .i_prg_num ( i_prg_num ),
        .o_data    ( o_rom     )
    );

    localparam ST_IDLE = 0, ST_BUSY = 1, ST_VALID = 2;

    reg [1:0]  state;


    always @(negedge i_clk or negedge i_rst_n) begin
        if (!i_rst_n) begin
            state <= ST_IDLE;
        end else begin
            case (state)
                ST_IDLE: begin
                    if (i_req) begin
                        case(i_cmd)
                        `MEM_WR: begin
                            state       <= ST_BUSY;
                            mem[i_addr] <= i_data;
                        end
                        `MEM_RD: begin
                            state       <= ST_BUSY;
                            if (i_addr >= 16)
                                o_reg <= mem[i_addr];
                            else
                                o_reg <= o_rom;
                        end
                        default:
                            $display("default case #2 memory rack");
                        endcase
                    end
                end
                ST_BUSY: begin
                    state <= ST_VALID;
                end
                ST_VALID: begin
                    state <= ST_IDLE;
                end
                default: begin
                    $display("In memory_rack default case #1");
                end
            endcase
        end
    end

    assign o_valid = state == ST_VALID ? 1 : 0;

    assign o_data = o_reg;
   
endmodule
