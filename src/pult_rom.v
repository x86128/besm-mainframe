module pult_rom(
    input  [14:0] i_addr,
    input  [3:0]  i_prg_num,
    output [47:0] o_data
);

    reg [47:0] rom[0:15][0:15];

    integer i;

    initial begin
        rom[0][1]   = 48'o00100001_00100002;
        rom[0][2]   = 48'o00100003_03300000;
        rom[0][3]   = 48'o00100410_03300410;
        rom[0][4]   = 48'o10100011_13300011;
    end

    assign o_data = rom[i_prg_num][i_addr[3:0]];

endmodule