`include "besm_cpu.vh"
`include "memory_rack.vh"
`timescale 100ns / 100ps
`default_nettype none

module besm_cpu
(
    input 	 i_clk,
    input 	 i_rst_n,

    output reg [14:0] mem_addr,
    output reg        mem_cmd,
    output reg        mem_req,
    input             mem_valid,
    input  [47:0]     i_data,
    output [47:0]     o_data,

    
    output 	 o_halted 	 
);

    //reg [14:0]	mem_addr;                                        
    //reg 		mem_cmd;

    reg [14:0] 	pc;                                            
    reg [47:0]  cpu_acc;
    reg [4:0] 	state;
    reg 		o_halted;
    reg [47:0] 	iir;               // intermediate instr register

    // index registers
    wire [14:0] m_q;
    reg  [14:0] m_d;
    wire [3:0]  m_raddr;
    reg  [3:0]  m_waddr;
    reg         m_we;

    // index registers (modifiers)
    // sync write, async read
    mregs mregs
    (
        .i_clk   ( i_clk   ), 
        .i_we    ( m_we    ), 
        .wr_addr ( m_waddr ), 
        .rd_addr ( m_raddr ), 
        .i_data  ( m_d     ), 
        .o_data  ( m_q     )
    );  

    // for instruction decoder step
    reg 		 ppk;
    wire [23:0]  instr;

    
    assign instr     = !ppk ? iir[47:24] : iir[23:0];
    
    // decode logic
    wire         is_long;
    wire         is_mag;
    wire         arg_addr;
    wire [3:0] 	 op_idx;
    wire [6:0]   op_code;
    wire [14:0]  op_addr;

    // decode unit
    besm_decode besm_decode
    (
        .instr   ( instr   ),
        .is_long ( is_long ),
        .is_mag  ( is_mag  ),
        .op_idx  ( op_idx  ),
        .op_code ( op_code ),
        .op_addr ( op_addr )
    );

    assign m_raddr   = op_idx;
    assign arg_addr  = op_addr + m_q;
    
    initial begin
        $monitor("MEM: %3d %o %o %o %o %o",$time,mem_addr, mem_cmd, mem_req, mem_valid, i_data);
    end

	always @(posedge i_clk or negedge i_rst_n) begin
        if (!i_rst_n) begin
            state     <= `ST_FETCH0;
            o_halted  <= 0;
	        pc        <= 1; // executing starts at 1 address
	        mem_cmd   <= `MEM_RD;
            mem_req   <= 0;
	        ppk       <= 0;
	        $display("CPU reset");
        end
        else begin
            $display("Begin case state: %d", state);
            case(state) 
                `ST_FETCH0: begin
                    $display("in FETCH0 pc:%o", pc);
                    if (!ppk) begin
                        state     <= `ST_FETCH1;
                        mem_addr  <= pc;
                        mem_cmd   <= `MEM_RD;
                        mem_req   <= 1;
                        pc        <= pc + 1;
                    end else begin
                        state <= `ST_DECODE;
                    end
                end
                `ST_FETCH1: begin
                    $display("in FETCH1");
                    mem_req <= 0;
                    if (mem_valid) begin
                        state   <= `ST_DECODE;
                        iir     <= i_data;
                    end
                end
                `ST_DECODE: begin
                    $display("in DECODE ppk: %o IIR: %o %o %o %o", ppk, iir, op_idx, op_code, op_addr);
                    case(op_code)
                        `OP_STOP: begin
                            state <= `ST_STOP;
                        end
                        `OP_XTA:
                            state <= `ST_XTA0;
                        default: begin
                            $display("Unknown op_code: %o", op_code);
                            state <= `ST_INVALID;
                        end
                    endcase // case (opc_long)
                end
                `ST_XTA0: begin
                    $display("XTA_0: %o", is_mag);
                    if (is_mag) begin
                        m_d     <= m_q - 1;
                        m_we    <= 1;
                        m_waddr <= 15;
                    end
                    state <= `ST_XTA1;
                end
                `ST_XTA1: begin
                    $display("%o %o %o", m_d, m_q, m_waddr);
                    mem_addr <= m_q + op_addr;
                    mem_cmd <= `MEM_RD;
                    mem_req <= 1;
                    state <= `ST_XTA2;
                end
                `ST_XTA2: begin
                    $display("XTA_2: mem_addr: %o mem_we: %o i_data:%o", mem_addr, mem_cmd, i_data);
                    mem_req <= 0;
                    if (mem_valid) begin
                        cpu_acc <= i_data;
                        state <= `ST_EXECUTE;
                    end
                end
                `ST_EXECUTE: begin
                    $display("in EXECUTE ppk %o", ppk);
                    $display("ACC: %o", cpu_acc);
                    state <= `ST_FETCH0;
                    ppk <= ~ppk;
                end
                `ST_INVALID: begin
                    $display("in INVALID %o opcode=%o", instr, op_code);
                    state <= `ST_STOP;
                end
                `ST_STOP: begin
                    $display("in STOP state");
                    o_halted <= 1;
                    state <= `ST_STOP;
                    $display("ACC: %o", cpu_acc);
                end
                default: begin
                    state  <= `ST_STOP;
                end
            endcase
        end
    end
endmodule // besm_cpu
