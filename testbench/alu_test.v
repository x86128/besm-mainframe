module top();
  
  reg i_clk;
  reg i_rst;
  
  reg i_pbn;
  reg i_pbo;
  
  reg [47:0] acc;
  reg [47:0] rmr;
  
  reg i_valid;
  
  wire [47:0] o_acc;
  wire [47:0] o_rmr;
  wire o_valid;

  wire [6:0] exp;
  wire sig;
  wire [39:0] mant;
  
  norm nrmlzr(.i_clk(i_clk),     .i_rst(i_rst),
              .i_pbn(i_pbn),     .i_pbo(i_pbo),
              .i_acc(acc),       .i_rmr(rmr),
              .i_valid(i_valid),
              .o_acc(o_acc), .o_rmr(o_rmr), .o_valid(o_valid));
  
  initial begin
    $dumpvars;
    $monitor("e: %b s: %b m: %b v:%b rmr: %b", exp, sig, mant, o_valid, o_rmr[39:0]);
    i_clk = 0;
    i_rst = 0;
    i_pbn = 0;
    i_pbo = 0;
    i_valid = 0;
    acc = {7'b0010010, 1'b0, 40'o1000000000000};
    rmr = 48'o1;
    #1  i_rst = 1;
    #10 i_rst = 0;
    #1 i_valid = 1;
  end
  
  always begin
    #1 i_clk <= ~i_clk;
    
  end
  
  always @(o_valid) begin
    if (o_valid) $finish;
  end

  assign {exp,sig,mant} = {o_acc[47:41], o_acc[40], o_acc[39:0]};
    
endmodule