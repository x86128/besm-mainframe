`timescale 100ns / 100ps
`default_nettype none

module top();

    reg  clk;                                                    
    reg  rst;                                                    

    reg  [3:0]  prg_num; // hardwired program selector

    wire [14:0] mem_addr;
    wire        mem_cmd;
    wire        mem_req;
    wire        mem_valid;

    wire [47:0] i_data;
    wire [47:0] o_data;
    wire halt;


    memory_rack memory_rack
    (
        .i_clk     ( clk      ),
        .i_rst_n   ( rst      ),
        .i_addr    ( mem_addr ),
        .i_req     ( mem_req  ),
        .i_cmd     ( mem_cmd  ),
        .o_valid   ( mem_valid),
        .i_data    ( o_data   ),
        .o_data    ( i_data   ),
        .i_prg_num ( prg_num  )
    );

    besm_cpu cpu
    (
        .i_clk    ( clk      ),
        .i_rst_n  ( rst      ),
        .mem_addr ( mem_addr ),
        .mem_req  ( mem_req  ),
        .mem_cmd  ( mem_cmd  ),
        .mem_valid( mem_valid),
        .o_data   ( o_data   ),
        .i_data   ( i_data   ),
        .o_halted ( halt     )
    );


    initial begin
        prg_num = 0;
        clk = 0;
        rst = 0;
        #1 rst = 1;
        #200 $finish;
    end                                                         

    always begin                                                
        #1 clk = ~clk;
        if (halt) $finish;
    end                                                         

    initial begin                                               
        $dumpvars;                                                  
    end                                                         
endmodule                                                    
