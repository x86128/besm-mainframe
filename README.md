# Verilog implementation of BESM-6 mainframe ISA

In this project I am trying to build Verilog/SystemVerilog model of BESM-6 soviet mainframe CPU in parallel of my studying of VHDL languages as hobby pet-project.

## Some interesting facts about BESM-6 mainframe

Machine have 7 input/output channels:

* 1,2 of which is dedicated to magnetic drums
* 3,4 to magnetic disk drives
* 5 to magnetic tape drives of EC-type
* 6 to native BESM magnetic tape drives
* 7 channel used to control slow devices such as terminals
  
Standard BESM-6 CPU can direct address only 32K words of core memory, but with supervisor-controlled special page mapping registers, this amount can be expanded to 128K words of addressable memory.

Machine has special caching scheme between instruction prefetch unit and data load/store unit, which allows to reduce access times to slow core memory in small control/computing loops. Also, this scheme improves stack based calculations with data stack deep about 8 real number.

CPU contains 15 index registers which provides fast indirect operand addressing modes, also chain indirect addressing.

CPU has support for "macrocommands" (for calculating sin,tan, sqrt and so), also known as soft interrupts in modern literature. Macrocommand implementation is part of supervisor or language support library.

Machine has 48-bit data word with additional special bits for parity check and program/data execution protection.

Computer number format has 40 bit mantissa, sign bit and 7 bit biased exponent without hidden bit in mantissa.

Machine has pipelined structure which allows parallel execution of ALU and control operations. It's average speed about of 1M ops per sec, with average latnecy of ALU operations about 0,5 to 5 microseconds.

Instruction size is 24 bit which packed by 2 to 48 bit memory word. It's one address typed. ISA consists of about 50 machine commands plus macrocommands.

## Input/output devices

Magnetic drums:

* total capacity - 256K words of 50 bit
* number of drums - 8 (up to 16)
* block size - 32K words
* data transfer size - 256 (paragraph) words and 1024 (page)

Hard disks:

* capacity - 4096K words
* number of drives - 4 (up to 16)
* one drive capacity - 1024K words
* transfer size - 512 or 1024 words

EC-type magnetic tapes:

* total capacity - 8M words
* number of drives - 8
* write block size - 256 or 1024 words
* read block size - up to 1024 words
  
BESM-native tape drives:

* total capacity - 4M of 50-bit words
* number of drives - 4 (up to 16)
* capacity of one tape - 1M words
* read or write block size - 1024 words

Machine can simultaneously transfer data from 2 drums, 2 tapes and 2 disk drives. Search for tape record marks can be done on all tape drives simultaneously.

Standard equipment set of BESM-6 system:

* 2 punch card readers
* 1 paper tape reader
* 2 ACPU line drum priners
* 2 card punch machines
* 1 paper punch machine
* 20 telegraph channels
* 2 punch card typewriter
* 1 punch tape typewriter
